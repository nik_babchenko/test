$(function () {
    var quantity,
        rawData;
    $.ajax({
        url: 'http://localhost:8080/api/v1/config',
        dataType: "jsonp",
        success: function (data) {
            qty = data.POINTS.QTY;
            min = data.POINTS.MIN;
            max = data.POINTS.MAX;
            updateInterval = data.POINTS.UPDATE_INTERVAL;
            $.ajax({
                url: 'http://localhost:8080/api/v1/points',
                dataType: "jsonp",
                success: function (data) {
                    rawData = data;
                    drawGraph();
                    setInterval(function(){
                        drawGraph()}, 1000);
                }
            });

        }
    });


    function drawGraph() {
        //drawing graph
        var height = 500,
            width = 1000,
            margin = 30,
            data=[];


        // creating X axis
        var xAxisLength = width - 2 * margin;

        // creating Y axis
        var yAxisLength = height - 2 * margin;

        // X axis interpolation
        var scaleX = d3.scale.linear()
            .domain([0, qty])
            .range([0, xAxisLength]);

        // Y axis interpolation
        var scaleY = d3.scale.linear()
            .domain([max, min])
            .range([0, yAxisLength]);


        // creating X
        var xAxis = d3.svg.axis()
            .scale(scaleX)
            .orient("bottom");

        // creating Y
        var yAxis = d3.svg.axis()
            .scale(scaleY)
            .orient("left");

        // if it is first iteration
        if (!$('.line').length) {
            // creating svg obj
            var svg = d3.select("body").append("svg")
                .attr("class", "axis")
                .attr("width", width)
                .attr("height", height);


            // scale Data
            for(i=0; i<rawData.length; i++) {
                data.push({
                    x: scaleX(i) + (margin + 1),
                    y: scaleY(rawData[i]) + margin
                })
            }

            // drawing X axis
            svg.append("g")
                .attr("class", "x-axis")
                .attr("transform",
                    "translate(" + margin + "," + (height - margin) + ")")
                .call(xAxis);


            // drawing Y axis
            svg.append("g")
                .attr("class", "y-axis")
                .attr("transform",
                    "translate(" + margin + "," + margin + ")")
                .call(yAxis);

            // drawing vertical grid lines
            d3.selectAll("g.x-axis g.tick")
                .append("line")
                .classed("grid-line", true)
                .attr("x1", 0)
                .attr("y1", 0)
                .attr("x2", 0)
                .attr("y2", - (yAxisLength));

            // drawing horizontal grid lines
            d3.selectAll("g.y-axis g.tick")
                .append("line")
                .classed("grid-line", true)
                .attr("x1", 0)
                .attr("y1", 0)
                .attr("x2", xAxisLength)
                .attr("y2", 0);

            // function that creates lines from array
            var line = d3.svg.line()
                .x(function(d){return d.x;})
                .y(function(d){return d.y;});


            // adds path
            svg.append("path")
                .attr("class", "line")
                .attr("d", line(data))
                .style("stroke", "steelblue")
                .style("stroke-width", 2);

        }
        else {

            $.ajax({
                url: 'http://localhost:8080/api/v1/points',
                dataType: "jsonp",
                success: function (data) {
                    console.log(data);
                    var points = data;
                    var svg = d3.select('body').transition();
                    var line = d3.svg.line()
                        .x(function (d) {
                            return d.x;
                        })
                        .y(function (d) {
                            return d.y;
                        });
                    data = [];
                    for (i = 0; i < points.length; i++) {
                        data.push({
                            x: scaleX(i) + (margin + 1),
                            y: scaleY(points[i]) + margin
                        });
                    }

                    svg.select("path.line")   // change the line
                        .duration(750)
                        .attr("d", line(data));
                }

            });

        }
    }


});
